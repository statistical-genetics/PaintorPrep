from optparse import OptionParser
import pandas as pd
import numpy as np
import time
from pathlib import Path as path
import sys
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook


def calcProb(locus):
    locus = pd.read_csv(locus,index_col=False, sep=' ')

    sorted = locus.sort_values(by="Posterior_Prob", ascending=True, kind = 'quicksort', ignore_index=True)



    return None


def main() -> int:

    #TODO use programme without writing tons of files + give choice to user if want to write
    parser = OptionParser()
    parser.add_option("-l", "--locus", dest="loc")   #data bank file directory and name                #header name of Zscore new column, "Zscore" recommended for PAINTOR
    (options, args) = parser.parse_args()


    locus= options.loc
   
    calcProb(locus)

    return 0


#if __name__ == "__main__": main()
