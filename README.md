# Work In Progress
## main.py:

main.py script separates genomic data into separate loci ready for use in PAINTOR and its additional scripts (LD matrix script and annotation script)
example :

from a genomic data bank, specify the data bank file name, separator, P-value header, Standart error header, effect header, chromosome header, effect allele header, alternative allele header, SNP position header, and name of wanted Zscore header (default recommended for PAINTOR)

`python main.py -d data/input/CAD_META --sp "\t" --pv Pvalue --st StdErr -e Effect --chr CHR --a1 "Allele1" --a2 "Allele2" --pos BP -z "Zscore"` 

with `CAD_META` file looking like this :

```
MarkerName	Allele1	Allele2	Freq1	FreqSE	MinFreq	MaxFreq	Effect	StdErr	Pvalue	Direction	HetISq	HetChiSq	HetDf	HetPVal	oldID	CHR	BP
10:100000625_A_G	a	g	0.5604	0.0081	0.5499	0.5667	0.0264	0.0056	2.59e-06	++	13	1.15	1	0.2835	rs7899632	10	100000625
10:100000645_A_C	a	c	0.806	0.0089	0.7996	0.8184	-0.0119	0.0071	0.09448	--	0	0.578	1	0.4469	rs61875309	10	100000645
10:100001867_C_T	t	c	0.0129	7e-04	0.0114	0.0132	0.0296	0.0293	0.3126	-+	0	0.485	1	0.4861	rs150203744	10	100001867
10:100003242_G_T	t	g	0.88	0.0034	0.8756	0.8827	0.0107	0.0086	0.2136	++	0	0.002	1	0.9642	rs12258651	10	100003242
10:100003304_A_G	a	g	0.9636	0.0037	0.9615	0.9701	0.0066	0.0158	0.6787	-+	53.3	2.142	1	0.1433	rs72828461	10	100003304
10:100003785_C_T	t	c	0.6432	6e-04	0.6425	0.6437	-0.0203	0.0058	0.0004992	--	0	0.174	1	0.6768	rs1359508	10	100003785
10:100004360_A_G	a	g	0.1939	0.009	0.1814	0.2004	0.012	0.0071	0.0922	++	0	0.608	1	0.4355	rs1048754	10	100004360
10:100004441_C_G	c	g	0.6328	0.0029	0.629	0.635	-0.0196	0.0058	0.0007328	--	0	0.117	1	0.7318	rs1048757	10	100004441
10:100004799_A_C	a	c	0.9863	0.0014	0.9857	0.9891	-0.0185	0.0267	0.4873	--	30.6	1.441	1	0.2299	rs77264786	10	100004799
```

outputs multiple loci files looking like this :

```
CHR BP oldID Allele1 Allele2 Zscore
1 109321537 rs1999522 t c -0.09392265193370164
1 109321615 rs1767025 t c 1.5606060606060606
1 109321824 rs1759481 t g -1.5606060606060606
1 109322296 rs75919641 a c 2.0042553191489363
1 109322297 rs1690734 t g -1.5606060606060606
1 109322824 rs41278488 t g -2.2443181818181817
1 109322989 rs35147910 a g -1.5606060606060606
1 109323270 rs1690736 t g -2.753846153846154
1 109323275 rs1767026 a g -1.696969696969697
```
(also calculates Zscore:  Zscore = effect / stdErr)



### steps to using Paintor:

1. Make loci
2. Make LD matrices 
   - VCF
   - map file,  .bed
    - locus info
3. Annotate loci  (annotate **after** making matrices with provided script because the scripts drops lines and Paintor will not work if the number of lines is not the same as the anntations and the size of the LD matrix)

4. run PAINTOR


# TODO list
- [ ] FINISH README
- [ ] fix: SNP qui se retrouvent plusieurs fois dans différents loci
- [ ] fix: annotations tout le temps à 0
- [x] trier par ordre croissant les loci donnés par main.py
- [ ] automatiser la création de dossier nécessaires
- [ ] vérifier que l'ordre des allèles corresponds au VCF de référence
- [ ] Analyser les résultats:
    - [ ] regarder la répartition des probabilités à posteriori
    - [ ] Manhattan plot des SNP pour tous les chromosomes
    - [ ] plot de la répartition des post proba



 
