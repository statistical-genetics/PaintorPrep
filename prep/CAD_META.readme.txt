MarkerName	= Unique SNP identifier based on chr, position and the alleles (alphabetically ordered)
Allele1	= Effect allele
Allele2 = Other allele	
Freq1 = Frequency	
FreqSE = Frequency (Standard error)	
MinFreq	= Frequency (min)
MaxFreq	= Frequency (max)
Effect = Effect size	
StdErr = Standard error	
P-value = P-value	
Direction = Direction in UK Biobank or Cardiogram (in this order) 	
HetISq = heterogeneity Isq
HetChiSq = 	heterogeneity Chisq
HetDf = 	heterogeneity degrees of freedom
HetPVal = heterogeneity P
oldID = rs'id	
CHR = chromosome	
BP = position