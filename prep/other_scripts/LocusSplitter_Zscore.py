import pandas as pd
import numpy as np
import time
from pathlib import Path as path




#reading data
data_bank = pd.read_csv("CAD_META",index_col=False, sep='\t')
pseuil = 5e-08
locus_nb = 0
locus = pd.DataFrame(None)

start_time = time.time()

#TODO Ne pas prendre en compte les SNP significatif s'ils se trouvent déjà dans un locus
#     Ne pas parcourir les SNP qui était dans le locus précédent (en gros sauter de 500kb)
#     NE PRENDRE QUE SUR LE MEME CHROMOSOME
#     Verifier si la derniere ligne est dans la range du premier for()


for line in range(len(data_bank)):
    
    #print("ligne: "+str(line)+"\n")
    if data_bank.loc[line].Pvalue < pseuil:
        

        
        
        if len(locus)==0 or (not int(data_bank.loc[line].BP) in (locus['BP']).values):


            #print("INF à p seuil !")
            locus_nb = locus_nb + 1
            print(locus_nb)
            


            kbrange = range((data_bank.loc[line].BP) - 500000, (data_bank.loc[line].BP) + 500000 +1) 
        

            locus = data_bank.loc[data_bank['BP'].isin(kbrange)]



            locus['Zscore']=locus['Effect']/locus['StdErr']
            
            locus.drop(inplace=True, columns=['MarkerName', 'Freq1', 'FreqSE', 'MinFreq', 'MaxFreq', 'Effect', 'StdErr', 'Pvalue', 'Direction', 'HetISq', 'HetChiSq', 'HetDf', 'HetPVal'])
            data_bank = data_bank[['CHR','BP','oldID', 'Allele1', 'Allele2','Zscore']]
            
            locus.to_csv(f"locus_output/locus{locus_nb}", index=False, sep=',')
    else: None
        #print("sup à seuil")

print("--- %s secondes ---\n" % (time.time() - start_time))

print("nombre de locus :" + str(locus_nb))
        






















#print("Done !")







"""

sample = data_bank.loc[data_bank['BP'].isin(range(100000625,100003785))]



sample.to_csv("sample_test",index=False, sep=',')
"""


"""
for line in data_bank:
    if line.Pvalue < pseuil :
        #selectionner les SNP situé à +- 500kb du SNP significatifs
"""