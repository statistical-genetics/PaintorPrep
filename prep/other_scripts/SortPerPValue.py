import pandas as pd
import numpy as np
import time
from pathlib import Path as path


def sort(i: int) -> pd.DataFrame:
    #reading chromosome data
    print("reading...")

    #i -> chr index
    i = 22
    data = pd.read_csv(f"chromosomes/CHR{i}",index_col=False, sep='\t')
    print("done reading.\n")


    start_time = time.time()

    quicksorted = data.sort_values(by='Pvalue', ascending=True, kind = 'quicksort', ignore_index=True)

    print("--- %s secondes ---\n" % (time.time() - start_time))


    quicksorted.to_csv(f"chromosomes/quicksortedCHR{i}", index=False, sep=',')
 
