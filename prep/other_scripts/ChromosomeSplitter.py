from sqlite3 import DatabaseError
import pandas as pd
import numpy as np
import time
from pathlib import Path as path


#Splits Data bank into smaller files corresponding to each chromosome


print("starting...")
start_time = time.time()


#reading data
print("reading data...")
data_bank = pd.read_csv("CAD_META",index_col=False, sep='\t')
print("data read !")

#temporary pandas DataFrame to store snp data corresponding to current chromosome
chr = pd.DataFrame(None)


#building chromosome files
for i in range(1,22+1):
    print("Building chromosome %s file..." % i)
    chr = data_bank[data_bank['CHR'] == i]
    chr.to_csv(f"chromosomes/CHR{i}", index=False, sep='\t')


print("Done !")

print("--- done splitting chromosomes in %s seconds ---\n" % (time.time() - start_time))

print("Chromosome number :" + str(i))

#check if files were actually generated