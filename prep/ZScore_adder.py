import pandas as pd
from pathlib import Path


#reading data
data_bank = pd.read_csv("CAD_META_sample",index_col=False, sep='\t')

#adding and calculating the Zscore column
data_bank['Zscore']=data_bank['Effect']/data_bank['StdErr']

#Deleting unused columuns in CalcLD_1KG_VCF.py
data_bank.drop(inplace=True, columns=['MarkerName', 'Freq1', 'FreqSE', 'MinFreq', 'MaxFreq', 'Effect', 'StdErr', 'Pvalue', 'Direction', 'HetISq', 'HetChiSq', 'HetDf', 'HetPVal'])

#reorganizing columns as in CalcLD_1KG_VCF.py's doumentation
data_bank = data_bank[['CHR','BP','oldID', 'Allele1', 'Allele2','Zscore']]

data_bank.to_csv('output/CAD_META__ZScore',index=False, sep=',')